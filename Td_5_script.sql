--partie 1

--Nombre de patient ont été hospitalisés, au global et par années.

SELECT extract(YEAR from s.date_sortie)  AS annee, COUNT(*) AS nombre_patient_hospitalises_par_annee
FROM sejour s
where s.date_sortie > s.date_entree 
GROUP BY extract(YEAR from s.date_sortie) 
ORDER BY annee;

--Nombre de patient décédés avant la  sortie(mode de sortie = 9) ainsi que l'âge moyen au deces.

SELECT COUNT(*) AS nombre_deces, AVG(EXTRACT(YEAR FROM AGE(s.date_sortie, p.date_naissance))) AS age_moyen_deces
FROM sejour s
JOIN patient p ON s.id_patient = p.id_patient
WHERE s.mode_sortie = '9';

--Nombre de RUM par UF et  la duree moyenne de passage dans les UF (avec date_entree et date_sortie)
SELECT r.code_uf, COUNT(*) AS nombre_RUM , AVG(EXTRACT(day FROM AGE(r.date_sortie, r.date_entree))) AS moyenne_duree_RUM
FROM rum r
JOIN sejour s  ON s.id_sejour = r.id_sejour
GROUP BY r.code_uf;

-- Distribution des diagnostics principaux, diagnostics secondaires et des actes

SELECT rd.type_diagnostic, COUNT(*) AS nombre_diagnostic_principal
FROM rum_diagnostic rd
GROUP BY rd.type_diagnostic
ORDER BY nombre_diagnostic_principal DESC;

-- distribution des actes
SELECT ra.code_acte, COUNT(*) AS nombre_acte
FROM rum_acte ra 
GROUP BY ra.code_acte
ORDER BY nombre_acte DESC;

--partie 2
---- Combien d'UF différentes sont traversées au cours d'un séjour ? Med, Q1, Q3, min, max
WITH UF_Par_Sejour AS (
  SELECT s.id_sejour, COUNT(DISTINCT r.code_uf) AS nb_UF
  FROM sejour s
  JOIN rum r ON s.id_sejour = r.id_sejour
  GROUP BY s.id_sejour
)

SELECT
  COUNT(*) AS nombre_sejours,
  PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY nb_UF) AS Q1,
  PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY nb_UF) AS Mediane,
  PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY nb_UF) AS Q3,
  MIN(nb_UF) AS Min,
  MAX(nb_UF) AS Max
FROM UF_Par_Sejour;

-- Quelles sont les unités traversées en premier ?
WITH PremierUF AS (
  SELECT s.id_sejour, r.code_uf, MIN(r.date_entree) AS premiere_date_entree
  FROM sejour s
  JOIN rum r ON s.id_sejour = r.id_sejour
  GROUP BY s.id_sejour, r.code_uf
)

SELECT p.id_sejour, p.code_uf AS premiere_UF
FROM PremierUF p
WHERE p.premiere_date_entree = (
  SELECT MIN(pu.premiere_date_entree)
  FROM PremierUF pu
  WHERE pu.id_sejour = p.id_sejour
);

--Quelles sont les unités traversées en dernier ?
WITH DernierUF AS (
  SELECT s.id_sejour, r.code_uf, MAX(r.date_sortie) AS derniere_date_sortie
  FROM sejour s
  JOIN rum r ON s.id_sejour = r.id_sejour
  GROUP BY s.id_sejour, r.code_uf
)

SELECT d.id_sejour, d.code_uf AS dernier_UF
FROM DernierUF d
WHERE d.derniere_date_sortie = (
  SELECT MAX(du.derniere_date_sortie)
  FROM DernierUF du
  WHERE du.id_sejour = d.id_sejour
);

-- Dans le sous-groupe de séjours avec un seul RUM : descriptions des UFs, modes d'entrée, modes de sortie, et durée du RUM.
SELECT
  r.code_uf AS description_UF,
  r.mode_entree AS mode_entree,
  r.mode_sortie AS mode_sortie,
  EXTRACT(day FROM AGE(r.date_sortie , r.date_entree))AS duree_RUM
FROM rum r
INNER JOIN (
  SELECT id_sejour
  FROM rum
  GROUP BY id_sejour 
  HAVING COUNT(*) = 1
) subquery ON r.id_sejour = subquery.id_sejour;

--Comparaison des patients décédés et vivants : un seul rum ou plusieurs rums ?

SELECT
  CASE
    WHEN s.mode_sortie = '9' THEN 'Décédé'
    ELSE 'Vivant'
  END AS statut_patient,
  CASE
    WHEN subquery.nb_RUM = 1 THEN 'Un RUM'
    ELSE 'Plusieurs RUMs'
  END AS nombre_RUM,
  COUNT(distinct s.id_sejour) AS nombre_patients
FROM sejour s 
LEFT JOIN (
  SELECT s.id_sejour, COUNT(DISTINCT r.id_sejour) AS nb_RUM
  FROM sejour s
  LEFT JOIN rum r ON s.id_sejour = r.id_sejour
  GROUP BY s.id_sejour
) subquery ON s.id_sejour = subquery.id_sejour
GROUP BY statut_patient, nombre_RUM;

--Compter les associations d'UFs pour les deux premiers RUMs

WITH DeuxPremiersRUMs AS (
  SELECT r.id_rum, r.code_uf
  FROM rum r
  WHERE r.id_rum IN (
    SELECT DISTINCT ON (r.id_rum) r.id_rum
    FROM rum r
    ORDER BY r.id_rum, r.date_entree
    LIMIT 2
  )
)

SELECT id_rum, code_uf, COUNT(*) AS nombre_associations_UF
FROM DeuxPremiersRUMs
GROUP BY id_rum, code_uf;

---- Compter les associations d'UFs pour toutes les transitions entre RUMs.
WITH TransitionsRUM AS (
  SELECT
    r1.id_sejour,
    r1.code_uf AS UF_RUM_Prec,
    r2.code_uf AS UF_RUM_Suivant
  FROM rum r1
  JOIN rum r2 ON r1.id_sejour = r2.id_sejour
    AND r1.date_sortie < r2.date_entree
)

SELECT UF_RUM_Prec, UF_RUM_Suivant, COUNT(*) AS nombre_transitions
FROM TransitionsRUM
GROUP BY UF_RUM_Prec, UF_RUM_Suivant
ORDER BY UF_RUM_Prec, UF_RUM_Suivant;

---- Compter les transitions du mode d'entrée vers le premier RUM.
WITH PremierRUM AS (
  SELECT s.id_sejour, r.mode_entree AS mode_entree_premier_RUM
  FROM sejour s
  LEFT JOIN rum r ON s.id_sejour = r.id_sejour
  WHERE r.date_entree = (
    SELECT MIN(r2.date_entree)
    FROM rum r2
    WHERE r2.id_sejour = s.id_sejour
  )
)

SELECT s.mode_entree AS mode_entree_patient, COUNT(*) AS nombre_transitions
FROM sejour s
JOIN PremierRUM pr ON s.id_sejour = pr.id_sejour
GROUP BY s.mode_entree
ORDER BY s.mode_entree;


---- Compter les transitions du dernier rum vers le mode de sortie.

WITH DernierRUM AS (
  SELECT s.id_sejour, r.mode_sortie AS mode_sortie_dernier_RUM
  FROM sejour s
  LEFT JOIN rum r ON s.id_sejour = r.id_sejour
  WHERE r.date_sortie = (
    SELECT MAX(r2.date_sortie)
    FROM rum r2
    WHERE r2.id_sejour = s.id_sejour
  )
)

SELECT s.mode_sortie AS mode_sortie_patient, COUNT(*) AS nombre_transitions
FROM sejour s
JOIN DernierRUM dr ON s.id_sejour = dr.id_sejour
GROUP BY s.mode_sortie
ORDER BY s.mode_sortie;

---- Compter les séquences complètes (ex : Urgences - UF1 - UF2 - Domicile = 5 séjours).


WITH Sequences AS (
  SELECT
    id_sejour,
    mode_entree,
    mode_sortie,
    ROW_NUMBER() OVER (PARTITION BY id_patient ORDER BY date_entree) AS row_num
  FROM sejour
)

SELECT COUNT(*) AS nombre_sequences_completes
FROM (
  SELECT id_sejour, mode_entree, mode_sortie, row_num
  FROM Sequences
  WHERE mode_entree = 'Urgences'
  
  UNION ALL

  SELECT s.id_sejour, s.mode_entree, s.mode_sortie, s.row_num
  FROM Sequences s
  JOIN Sequences prev ON s.id_patient = prev.id_patient AND s.row_num = prev.row_num + 1
  WHERE prev.mode_sortie = 'Urgences'
  
  UNION ALL

  SELECT id_sejour, mode_entree, mode_sortie, row_num
  FROM Sequences
  WHERE mode_sortie = 'Domicile'
) AS CompleteSequences



